match ErrorMsg '\s\+$'
"set colorcolumn=80

" Set to auto read when a file is changed from the outside
set autoread

" Ignore case when searching
set ignorecase

" When searching try to be smart about cases
set smartcase

" Highlight search results
set hlsearch

" Makes search act like search in modern browsers
set incsearch

" For regular expressions turn magic on
set magic

" Show matching brackets when text indicator is over them
set showmatch

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Colors and Fonts
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Enable syntax highlighting
syntax enable

colorscheme desert
set background=dark

" Set utf8 as standard encoding and en_US as the standard language
set encoding=utf8

" Use Unix as the standard file type
set ffs=unix,dos,mac

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Text, tab and indent related
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Use spaces instead of tabs
set expandtab

" Be smart when using tabs ;)
set smarttab

" 1 tab == 4 spaces
set shiftwidth=4
set tabstop=4

" Linebreak on 500 characters
set lbr
set tw=500

set ai "Auto indent
set si "Smart indent
set wrap "Wrap lines

" Delete trailing white space on save, useful for Python and ruby ;)
func! DeleteTrailingWS()
  exe "normal mz"
    %s/\s\+$//ge
    exe "normal `z"
endfunc

autocmd BufWrite *.c,*.h,*.py,*rb,*.pl :call DeleteTrailingWS()
autocmd BufRead,BufNewFile *.rb :set tabstop=2 shiftwidth=2
autocmd BufRead,BufNewFile Makefile,*.c,*.h,*.java set noic cin noexpandtab tabstop=8 shiftwidth=8

:if !empty($TMUX)
:   " BufEnter works better with tabs
:   autocmd BufEnter * call system("tmux rename-window " . expand("%:t"))
:   " TODO somehow restore previous name
:   autocmd VimLeave * call system("tmux rename-window bash")
:endif
